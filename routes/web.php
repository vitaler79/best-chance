<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/home/getprize', [App\Http\Controllers\HomeController::class, 'getPrize'])->name('home.getPrize');
Route::post('/home/refuse', [App\Http\Controllers\HomeController::class, 'refuse'])->name('home.refuse');
Route::post('/home/moneytoaccount', [App\Http\Controllers\HomeController::class, 'moneyToAccount'])->name('home.moneyToAccount');
Route::post('/home/bonustoaccount', [App\Http\Controllers\HomeController::class, 'bonusToAccount'])->name('home.bonusToAccount');
Route::get('/shipping/{id}', [App\Http\Controllers\ShippingController::class, 'index'])->where('id', '[0-9]+')->name('shipping');
Route::post('/shipping/query', [App\Http\Controllers\ShippingController::class, 'query'])->name('shipping.query');
Route::get('/bank/{id}', [App\Http\Controllers\BankController::class, 'index'])->where('id', '[0-9]+')->name('bank');
Route::post('/bank/query', [App\Http\Controllers\BankController::class, 'query'])->name('bank.query');
