<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PrizeProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('prize_products')->insert([
            'name' => 'Vacuum cleaner BOSH ZXC-1600',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('prize_products')->insert([
            'name' => 'iPhone 14',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('prize_products')->insert([
            'name' => 'iPad Air 12',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('prize_products')->insert([
            'name' => 'Mac Book 19',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('prize_products')->insert([
            'name' => 'Xiaomi Redme 22 Pro',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('prize_products')->insert([
            'name' => 'TV Samsung FUUL HD ASDV23L',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('prize_products')->insert([
            'name' => 'iWatch',
            'amount' => 1,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
