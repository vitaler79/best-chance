<?php
/**
 * Created by PhpStorm.
 * User: Vit
 * Date: 05.07.2019
 * Time: 23:46
 */
namespace App\Models;

interface RefusePrize
{
    public function refuse($prize);
}
