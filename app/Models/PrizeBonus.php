<?php
/**
 * Created by PhpStorm.
 * User: Vit
 * Date: 05.07.2019
 * Time: 22:53
 */

namespace App\Models;


class PrizeBonus implements ChoosePrize
{
    public function choose()
    {
        $maxBonusPrise = (int)env('MAX_BONUS_PRIZE',10000);
        $minBonusPrise = (int)env('MIN_BONUS_PRIZE',1000);
        return rand($minBonusPrise, $maxBonusPrise);
    }

    public function refuse($sum)
    {
        return $sum;
    }

    public function getPrize($id)
    {
        return $id;
    }


}
