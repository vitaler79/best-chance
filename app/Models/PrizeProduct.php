<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrizeProduct extends Model implements ChoosePrize, RefusePrize
{
    public function choose()
    {
        $product = self::inRandomOrder()->where('status', 0)->where('amount', '>', 0)->first();

        if(isset($product->id)) {
            $product->status = 1;
            $product->amount = $product->amount - 1;
            $product->save();

            return $product->id;
        } else {
            return 0;
        }
    }


    public function refuse($id)
    {
        if($id) {
            $product = self::where("id",$id)->first();
            $product->status = 0;
            $product->amount = $product->amount + 1;
            $product->save();

            return $product->id;
        } else {
            return 0;
        }
    }

    public function getPrize($id)
    {
        return self::where("id",$id)->first();
    }
}
