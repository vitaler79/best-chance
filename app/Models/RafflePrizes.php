<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RafflePrizes extends Model
{
    public function getLastRaffle($userId)
    {
        return self::latest('id')->where('user_id', $userId)->where('status', 0)->first();;
    }

    public function getRaffle($id, $userId)
    {
        return self::where('id', $id)->where('user_id', $userId)->where('status', 0)->first();
    }
}
