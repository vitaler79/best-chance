<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrizeMoney extends Model implements ChoosePrize, RefusePrize
{
    public function choose()
    {
        $maxMoneyPrise = (int)env('MAX_MONEY_PRIZE',1000);
        $minMoneyPrise = (int)env('MIN_MONEY_PRIZE',100);
        $prizeSum = rand($minMoneyPrise, $maxMoneyPrise);

        $maxTotal = PrizeMoney::latest('id')->first();
        if (isset($maxTotal->sum)) {

            if ($maxTotal->sum>=$prizeSum) {
                $maxTotal->sum = $maxTotal->sum - $prizeSum;
            } else {
                $prizeSum = $maxTotal->sum;
                $maxTotal->sum = 0;
            }
            $maxTotal->save();

            return $prizeSum;
        }

        return 0;
    }

    public function refuse($sum)
    {
        if($sum) {
            $maxTotal = PrizeMoney::latest('id')->first();
            $maxTotal->sum = $maxTotal->sum + $sum;
            $maxTotal->save();

            return $maxTotal->sum;
        } else {
            return 0;
        }
    }

    public function getPrize($id)
    {
        return self::where("id",$id)->first();
    }
}
