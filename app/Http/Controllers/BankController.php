<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\RafflePrizes;
use App\Models\Bank;
use App\Jobs\SendMoneyToBank;

class BankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $userId = \Auth::user()->id;

        if (isset($request->id)) {
            $raffleId = (int)$request->id;
            $rafflePrize = RafflePrizes::where('id', $raffleId)->where('prize_type', 'money')->where('user_id', $userId)->first();

            if (isset($rafflePrize->id)) {
                $bank = Bank::where('raffle_id', $raffleId)->where('user_id', $userId)->first();

                return view('bank', ['bank' => $bank, 'raffle' => $rafflePrize]);
            }
        }

        return redirect()->route('home');
    }


    public function query(Request $request)
    {


        $userId = \Auth::user()->id;

        if (isset($request->raffle_id)) {

            $raffleId = (int)$request->raffle_id;            

            SendMoneyToBank::dispatch($request, $userId);

            $rafflePrize = (new RafflePrizes())->getRaffle($raffleId, $userId);
            $rafflePrize->status = 3;
            $rafflePrize->save();
        }

        return redirect()->route('home');

    }
}
