<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PrizeFactory;
use App\Models\RafflePrizes;
use App\Models\PrizeProduct;
use App\Models\PrizeMoney;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Expression;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rafflePrize = (new RafflePrizes())->getLastRaffle(\Auth::user()->id);

        if(isset($rafflePrize->prize_type)) {
            $prizeObj = (new PrizeFactory())->make($rafflePrize->prize_type)->getPrize($rafflePrize->prize);
        } else {
            $prizeObj = '';
        }

        return view('home', ['raffle' => $rafflePrize, 'prize' => $prizeObj]);
    }

    public function getPrize(RafflePrizes $rafflePrize)
    {
        $userId = \Auth::user()->id;

        try{

            DB::beginTransaction();

            $prisesArr = explode(',', env('PRIZES_TYPES_FOR_RAFFLE', 'money,bonus,product'));
            $prisesArrLen = count($prisesArr);

            if ($prisesArrLen > 0) {
                $index = rand(0, $prisesArrLen-1);
                $prizeType = $prisesArr[$index];

                $prize = (new PrizeFactory())->make($prizeType)->choose();

                if ($prize) {
                    $rafflePrize->user_id = $userId;
                    $rafflePrize->prize_type = $prizeType;
                    $rafflePrize->prize = $prize;
                    $rafflePrize->status = 0;
                    $rafflePrize->save();
                }
            }

            DB::commit();

        } catch(\Exception $e){
            DB::rollback();

            echo $e->getMessage();
        }

        return redirect()->route('home');
    }


    public function refuse(Request $request)
    {
        try{

            DB::beginTransaction();

            if (isset($request->id)) {

                $rafflePrize = (new RafflePrizes())->getRaffle((int)$request->id, \Auth::user()->id);

                if(isset($rafflePrize->id)) {
                    $rafflePrize->status = 1;
                    $rafflePrize->save();

                    $prizeObj = (new PrizeFactory())->make($rafflePrize->prize_type);
                    $prizeObj->refuse($rafflePrize->prize);
                } else {
                    throw new \Exception('Wrong id');
                }

            }

            DB::commit();

        } catch(\Exception $e){
            DB::rollback();

            echo $e->getMessage();
        }

        return redirect()->route('home');
    }

    public function moneyToAccount(Request $request)
    {

        try{

            DB::beginTransaction();

            if (isset($request->id)) {

                $rafflePrize = (new RafflePrizes())->getRaffle((int)$request->id, \Auth::user()->id);

                if (isset($rafflePrize->id)) {
                    $rafflePrize->status = 2;
                    $rafflePrize->save();

                    $rate = (float)env("MONEY_TO_ACCOUNT", 2);

                    \Auth::user()->account = \Auth::user()->account + round($rate * $rafflePrize->prize);
                    \Auth::user()->save();
                } else {
                    throw new \Exception('Wrong id');
                }
            }

            DB::commit();

        } catch(\Exception $e){
            DB::rollback();

            echo $e->getMessage();
        }

        return redirect()->route('home');
    }

    public function bonusToAccount(Request $request)
    {

        try{

            DB::beginTransaction();

            if (isset($request->id)) {

                $rafflePrize = (new RafflePrizes())->getRaffle((int)$request->id, \Auth::user()->id);

                if (isset($rafflePrize->id)) {
                    $rafflePrize->status = 2;
                    $rafflePrize->save();

                    $rate = (float)env("BONUSES_TO_ACCOUNT",0.5);

                    \Auth::user()->account = \Auth::user()->account + round($rate * $rafflePrize->prize);
                    \Auth::user()->save();
                } else {
                    throw new \Exception('Wrong id');
                }
            }

            DB::commit();

        } catch(\Exception $e){
            DB::rollback();

            echo $e->getMessage();
        }

        return redirect()->route('home');
    }
}
