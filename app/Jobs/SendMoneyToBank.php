<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Models\RafflePrizes;
use App\Models\Bank;
use Illuminate\Foundation\Validation\ValidatesRequests;

class SendMoneyToBank implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request;
    private $userId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request, $userId)    
    {
        $this->request = $request->validate([
            'raffle_id' => 'required',
            'full_name' => 'required',            
            'account' => 'required',
            'email' => 'required|email',
        ]);

        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        

        try{

            DB::beginTransaction();

            $rafflePrize = RafflePrizes::where('id', $this->request['raffle_id'])->where('user_id', $this->userId)->where('status', 3)->first();

            if (isset($rafflePrize->id)) {
                    $this->request['_token'] = sha1($this->request['email'] . date('Y-m-d H:i:s'));
                    $this->request['sum'] = $rafflePrize->prize;
                    $this->request['user_id'] =  $this->userId;
                    $this->request['status'] = 0;
                    Bank::forceCreate($this->request);

                    $rafflePrize->status = 2;
                    $rafflePrize->save();                
            }

            DB::commit();

        } catch(\Exception $e){
            DB::rollback();

            echo $e->getMessage();
        }

    }
}
